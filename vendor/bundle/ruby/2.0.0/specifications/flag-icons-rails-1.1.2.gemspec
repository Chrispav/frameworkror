# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = "flag-icons-rails"
  s.version = "1.1.2"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["Eugene Garlukovich"]
  s.date = "2016-02-12"
  s.description = "    'flag-icons-rails' gem allows you to integrate sass version of\n    https://github.com/lipis/flag-icon-css into your Ruby/Rails project.\n"
  s.email = ["eugene.garlukovich@gmail.com"]
  s.homepage = "https://github.com/eugenegarl/flag-icons-rails"
  s.licenses = ["MIT"]
  s.require_paths = ["lib"]
  s.rubygems_version = "2.0.14.1"
  s.summary = "flag-icon-css sass gem for use in Ruby/Rails projects"

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<sass>, ["~> 3.2"])
    else
      s.add_dependency(%q<sass>, ["~> 3.2"])
    end
  else
    s.add_dependency(%q<sass>, ["~> 3.2"])
  end
end
