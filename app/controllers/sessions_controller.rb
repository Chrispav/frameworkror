class SessionsController < ApplicationController

  def new
  end

  def create
  	@user = User.find_by_email(params[:session][:email])
  	if @user && @user.authenticate(params[:session][:password])
  		# session[:user_id] = @user_id
      log_in @user
      flash[:success] = "Successfully logged in"
  		redirect_back_or @user
  	else
  	# 	redirect_to '/login'
      flash.now[:danger] = 'Invalid email/password combination' # Not quite right!
      render 'new'
    end  
  end

  def destroy
    session[:user_id] = nil 
    flash[:success] = "Successfully logged out"
    redirect_to root_url
  end
end
