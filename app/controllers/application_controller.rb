class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  include SessionsHelper

  before_filter :set_locale

  def set_locale
    # if cookies[:educator_locale] && I18n.available_locales.include?(cookies[:educator_locale].to_sym)
    #   l = cookies[:educator_locale].to_sym
    # else
    #   l = I18n.default_locale
    #   cookies.permanent[:educator_locale] = l
    # end
    #   I18n.locale = l
    I18n.locale = params[:locale] if params[:locale].present?
  end

  def default_url_options(options = {})
    {locale: I18n.locale}
  end
  # helper_method :current_user, :logged_in?

  # private

    # helper_method :current_user, :logged_in?

  # def current_user
  	# @current_user ||= User.find(session[:user_id]) if session[:user_id]
    # User.where(id: session[:user_id]).first
  ############
      # return unless session[:user_id]
      # @current_user ||= User.find(session[:user_id])
  # end

  # def logged_in?
      # current_user.nil?
  # end

  # def require_user
  # 	redirect_to '#' unless current_user
  	
  # end


end
